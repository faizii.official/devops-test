
# Define the title and options for the Whiptail dialog box
TITLE="Choose an option"
OPTIONS=("1" "BUILD IMAGES"
         "2" "Select that option"
         "3" "Select another option"
         "4" "Select yet another option")

# Use Whiptail to display the dialog box and capture the user's selection
CHOICE=$(whiptail --title "$TITLE" --menu "Select an option" 15 60 4 "${OPTIONS[@]}" 3>&1 1>&2 2>&3)

#build images
if [ $CHOICE == "1" ]
then 
opbuild=("1" "install docker"
         "2" "Build image with IaC dependency"
         "3" "Build image for docker"
         "4" "reserved")

# Use Whiptail to display the dialog box and capture the user's selection
build=$(whiptail --title "$TITLE" --menu "Select an option" 15 60 4 "${opbuild[@]}" 3>&1 1>&2 2>&3)

fi

if [ $CHOICE == "2" ]
then 
echo 2
fi

if [ $CHOICE == "3" ]
then 
echo 3
fi

if [ $CHOICE == "4" ]
then 
echo 4
fi
