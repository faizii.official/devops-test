## As per the requirements the task is completed and tested on my end.
## How to get started?

in this repo, you will find the shell script named run.sh  you just have to execute that script on your local machine (I have tested it on arch linux ==> manjaro) 
command: 'bash run.sh' or 'sh run.sh' or 'chmod 777 && ./run.sh'

## pre-requesties
1) binary whiptail (I have created a script run.sh in the way that it has the ability to autodetect OS and install accordingly but in case the script fails to install binary you can install it manually   example: apt install newt)
2) binary Helm (in IaC I have created a Helm chart as well as manifests to provision the environment on Kubernetes so to install the environment via Helm we will need binary for helm.
3) binary kubectl (kubectl is required by run.sh automated GUI-based script to provision the infra in k8s via manifests files)
4) Kubernetes cluster with metrics server enabled (I have also set up the HPA to autoscale the pods for react and flask as we know HPA will not work if HPA is not able to get metrics of pod)
5) binary AWS CLI (I have created a code that has ablity to provide the task definitions for ECS on AWS via our Automation script "run.sh", No worries if your AWS isn't configured our automation script will configure AWS on behalf of you)
6) binary Terraform (I have created a IaC Code to provision our task via  terraform what you just need to do is run our script and configure AWS and then navigate to provision/terraform )
7) binary docker-compose (I have created a docker-compose file that can provision infra after building an image and this can be automatically provisioned via run.sh Automator script.)

## docker files that I have mentioned and from where I have built that.
1) dexterquazi/flask (build with docker file path 'flask/Dockerfile' )
2) dexterquzi/react (build with docker file path 'react/Dockerfile' )
3) dexterquazi/reactk8s (build with docker file path 'build/buildForK8s/react/Dockerfile' )
3) dexterquazi/react-ecs(build with docker file path 'build/buildForIaC/react/Dockerfile' )

## infra can be provisioned via 
1) Terraform
2) AWS CLI/Cloudformation template
3) docker-compose
4) Kubernetes by manifests/by helm (liveness & readyness is tested on EKS with m4.large single node may or may not need to configure for minikube testing)

## feel free to ask me for making any changes 
## Regards: -FAIZAN QUAZI